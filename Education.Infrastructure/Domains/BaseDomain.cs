﻿namespace Education.Infrastructure.Domains
{
    public abstract class BaseDomain
    {
        public int Id { get; set; }
    }
}