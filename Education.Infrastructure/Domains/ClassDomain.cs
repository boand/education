﻿using System.Collections.Generic;

namespace Education.Infrastructure.Domains
{
    public class ClassDomain : BaseDomain
    {
        public byte Grade { get; set; }
        public char Letter { get; set; }

        public int SchoolId { get; set; }
        public virtual SchoolDomain School { get; set; }

        public virtual ICollection<StudentDomain> Students { get; set; }
    }
}