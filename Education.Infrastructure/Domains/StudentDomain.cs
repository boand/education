﻿using Education.Infrastructure.Enums;

namespace Education.Infrastructure.Domains
{
    public class StudentDomain : BaseDomain
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }

        public int ClassId { get; set; }
        public virtual ClassDomain Class { get; set; }
    }
}