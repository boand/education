﻿using System.Collections.Generic;

namespace Education.Infrastructure.Domains
{
    public class SchoolDomain : BaseDomain
    {
        public string Title { get; set; }

        public virtual ICollection<ClassDomain> Classes { get; set; }
    }
}