﻿using System;
using System.Linq;
using Education.Infrastructure.Domains;

namespace Education.Infrastructure.Services.Interfaces
{
    public interface IRepository<T> : IDisposable
        where T : BaseDomain
    {
        IQueryable<T> Table { get; }
        T Get(int id);
        void Add(T domain);
        void Update(T domain);
        void Delete(int id);
    }
}