﻿namespace Education.Infrastructure.Enums
{
    public enum Gender : byte
    {
        Male,
        Female
    }
}