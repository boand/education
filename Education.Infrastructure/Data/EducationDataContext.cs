﻿using System.Data.Entity;
using Education.Infrastructure.Domains;

namespace Education.Infrastructure.Data
{
    public class EducationDataContext : DbContext
    {
        public DbSet<SchoolDomain> Schools { get; set; }
        public DbSet<ClassDomain> Classes { get; set; }
        public DbSet<StudentDomain> Students { get; set; }
    }
}