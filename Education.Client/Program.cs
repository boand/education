﻿using System;
using Education.Client.Services;
using Education.Client.Services.Interfaces;
using Education.Infrastructure.Data;
using Education.Infrastructure.Domains;
using Education.Infrastructure.Services;

namespace Education.Client
{
    internal class Program
    {
        private static ICrud[] _cruds;

        private static void Main()
        {
            using (var dataContext = new EducationDataContext())
            {
                _cruds = new ICrud[]
                {
                    new Crud<SchoolDomain>(new SchoolService(), new EfRepository<SchoolDomain>(dataContext)),
                    new Crud<ClassDomain>(new ClassService(), new EfRepository<ClassDomain>(dataContext)),
                    new Crud<StudentDomain>(new StudentService(), new EfRepository<StudentDomain>(dataContext))
                };

                while (true)
                    try
                    {
                        RunService();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
            }
        }

        private static void RunService()
        {
            for (var i = 0; i < _cruds.Length; i++)
                Console.WriteLine($"{i + 1} - {_cruds[i].GetDescription}");

            int id;
            do
            {
                Console.WriteLine("Укажите номер сервиса");
            } while (!int.TryParse(Console.ReadLine(), out id));

            RunAction(_cruds[id - 1]);
        }

        private static void RunAction(ICrud crud)
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("0 - Вернуться назад");
                Console.WriteLine("1 - Вывести полный список");
                Console.WriteLine("2 - Добавить запись");
                Console.WriteLine("3 - Редактировать запись");
                Console.WriteLine("4 - Удалить запись");

                int id;
                do
                {
                    Console.WriteLine("Укажите номер действия");
                } while (!int.TryParse(Console.ReadLine(), out id));

                switch (id)
                {
                    case 0:
                        return;
                    case 1:
                        crud.GetAll();
                        break;
                    case 2:
                        crud.CreateDomain();
                        break;
                    case 3:
                        crud.EditDomain();
                        break;
                    case 4:
                        crud.DeleteDomain();
                        break;
                }
            }
        }
    }
}