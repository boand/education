﻿using System;
using Education.Client.Services.Interfaces;
using Education.Infrastructure.Domains;

namespace Education.Client.Services
{
    internal class ClassService : IDomainService<ClassDomain>
    {
        public string GetServiceDescription => "Работа с классами";

        public ClassDomain FillDomain()
        {
            var studentDomain = new ClassDomain();

            byte grade;
            do
            {
                Console.WriteLine("Укажите номер нового класса");
            } while (!byte.TryParse(Console.ReadLine(), out grade));

            studentDomain.Grade = grade;

            string letter;
            do
            {
                Console.WriteLine("Укажите букву нового класса");
                letter = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(letter));

            studentDomain.Letter = letter[0];

            int schoolId;
            do
            {
                Console.WriteLine("Укажите номер школы нового класса");
            } while (!int.TryParse(Console.ReadLine(), out schoolId));

            studentDomain.SchoolId = schoolId;

            return studentDomain;
        }

        public void FillDomain(ref ClassDomain domain)
        {
            byte grade;
            do
            {
                Console.WriteLine($"Укажите новый номер класса (Текущий - {domain.Grade})");
            } while (!byte.TryParse(Console.ReadLine(), out grade));

            domain.Grade = grade;


            string letter;
            do
            {
                Console.WriteLine("Укажите новую букву класса");
                letter = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(letter));

            domain.Letter = letter[0];

            int schoolId;
            do
            {
                Console.WriteLine("Укажите новый номер школы класса");
            } while (!int.TryParse(Console.ReadLine(), out schoolId));

            domain.SchoolId = schoolId;
        }

        public string ToString(ClassDomain domain)
        {
            return $"{domain.Grade} {domain.Letter}";
        }
    }
}