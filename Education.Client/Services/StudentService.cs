﻿using System;
using Education.Client.Services.Interfaces;
using Education.Infrastructure.Domains;
using Education.Infrastructure.Enums;

namespace Education.Client.Services
{
    internal class StudentService : IDomainService<StudentDomain>
    {
        public string GetServiceDescription => "Работа с учениками";

        public StudentDomain FillDomain()
        {
            var studentDomain = new StudentDomain();
            Console.WriteLine("Укажите имя нового ученика");
            studentDomain.Name = Console.ReadLine();

            int age;
            do
            {
                Console.WriteLine("Укажите возраст нового ученика");
            } while (!int.TryParse(Console.ReadLine(), out age));

            studentDomain.Age = age;

            int gender;
            do
            {
                Console.WriteLine("Укажите пол нового ученика (0 - муж, 1 - жен)");
            } while (!int.TryParse(Console.ReadLine(), out gender));

            studentDomain.Gender = (Gender) gender;

            int classId;
            do
            {
                Console.WriteLine("Укажите номер класса нового ученика");
            } while (!int.TryParse(Console.ReadLine(), out classId));

            studentDomain.ClassId = classId;

            return studentDomain;
        }

        public void FillDomain(ref StudentDomain domain)
        {
            Console.WriteLine($"Укажите новое имя ученика (Текущее - {domain.Name})");
            domain.Name = Console.ReadLine();

            int age;
            do
            {
                Console.WriteLine($"Укажите новый возраст ученика (Текущий - {domain.Age})");
            } while (!int.TryParse(Console.ReadLine(), out age));

            domain.Age = age;

            int gender;
            do
            {
                Console.WriteLine("Укажите новый пол ученика (0 - муж, 1 - жен)");
            } while (!int.TryParse(Console.ReadLine(), out gender));

            domain.Gender = (Gender) gender;

            int classId;
            do
            {
                Console.WriteLine("Укажите новый номер класса ученика");
            } while (!int.TryParse(Console.ReadLine(), out classId));

            domain.ClassId = classId;
        }

        public string ToString(StudentDomain domain)
        {
            return $"{domain.Name}. Возраст - {domain.Age}, {domain.Gender}";
        }
    }
}