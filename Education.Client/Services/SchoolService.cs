﻿using System;
using Education.Client.Services.Interfaces;
using Education.Infrastructure.Domains;

namespace Education.Client.Services
{
    internal class SchoolService : IDomainService<SchoolDomain>
    {
        public string GetServiceDescription => "Работа со школами";

        public SchoolDomain FillDomain()
        {
            Console.WriteLine("Укажите название новой школы");
            return new SchoolDomain {Title = Console.ReadLine()};
        }

        public void FillDomain(ref SchoolDomain domain)
        {
            Console.WriteLine($"Укажите новое название школы (Текущее - {domain.Title})");
            domain.Title = Console.ReadLine();
        }

        public string ToString(SchoolDomain domain)
        {
            return domain.Title;
        }
    }
}