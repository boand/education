﻿namespace Education.Client.Services.Interfaces
{
    internal interface ICrud
    {
        string GetDescription { get; }
        void GetAll();
        void CreateDomain();
        void EditDomain();
        void DeleteDomain();
    }
}