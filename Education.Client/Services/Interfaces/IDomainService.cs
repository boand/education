﻿using Education.Infrastructure.Domains;

namespace Education.Client.Services.Interfaces
{
    internal interface IDomainService<T> where T : BaseDomain
    {
        string GetServiceDescription { get; }
        string ToString(T domain);
        T FillDomain();
        void FillDomain(ref T domain);
    }
}