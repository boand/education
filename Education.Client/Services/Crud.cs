﻿using System;
using Education.Client.Services.Interfaces;
using Education.Infrastructure.Domains;
using Education.Infrastructure.Services.Interfaces;

namespace Education.Client.Services
{
    internal class Crud<T> : ICrud where T : BaseDomain
    {
        private readonly IDomainService<T> _domainService;
        private readonly IRepository<T> _repository;

        public Crud(IDomainService<T> domainService, IRepository<T> repository)
        {
            _repository = repository;
            _domainService = domainService;
        }

        public string GetDescription => _domainService.GetServiceDescription;

        public void GetAll()
        {
            foreach (var domain in _repository.Table)
            {
                Console.Write(domain.Id);
                Console.Write(" - ");
                Console.WriteLine(_domainService.ToString(domain));
            }
        }

        public void CreateDomain()
        {
            var domain = _domainService.FillDomain();
            _repository.Add(domain);
        }

        public void EditDomain()
        {
            int id;
            do
            {
                Console.WriteLine("Укажите номер редактируемой записи");
            } while (!int.TryParse(Console.ReadLine(), out id));

            var domain = _repository.Get(id);
            _domainService.FillDomain(ref domain);
            _repository.Update(domain);
        }

        public void DeleteDomain()
        {
            int id;
            do
            {
                Console.WriteLine("Укажите номер удаляемой записи");
            } while (!int.TryParse(Console.ReadLine(), out id));

            _repository.Delete(id);
        }
    }
}